<?php
namespace Wise\Fancy\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface {
    protected $categorySetupFactory;

    public function __construct(\Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory) {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '2.1.0') < 0) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

            $categorySetup->addAttribute($entityTypeId, 'fancy_label', array(
                'type' => 'varchar',
                'label' => 'Fancy label',
                'input' => 'imagefile',
                'required' => false,
                'visible_on_front' => true,
                'apply_to' => 'simple',
                    //configurable,virtual,bundle,downloadable
                'unique' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_WEBSITE,
                'group' => 'Fancy'
            ));
        }
    }
}