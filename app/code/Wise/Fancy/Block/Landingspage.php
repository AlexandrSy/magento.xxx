<?php
namespace Wise\Fancy\Block;

use Magento\Framework\View\Element\Template;

class Landingspage extends Template
{
    public function getLandingsUrl()
    {
        return $this->getUrl('fancybox');
    }

}