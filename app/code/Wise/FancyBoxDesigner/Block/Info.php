<?php
/**
* Get Header from configuration value
* @return string
*/
public function getHeader()
{
  return $this->_scopeConfig->getValue('fancy/fancypage/header', ScopeInterface::SCOPE_STORE);
}